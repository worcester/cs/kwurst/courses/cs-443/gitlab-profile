# Blogs

- Your blog must be posted between the start date/time and end date/time of a particular week.
- You may only post one blog per week (you may post a second if you use a token).
- Your blog must be tagged to appear on the [CS@Worcester blog](http://cs.worcester.edu/blog/).
- Your blog must be tagged with the appropriate week tag as given in the table below.
- Your blog must be tagged with the course number `CS-443`

See the [Professional Development Blog Entry Specification](https://gitlab.com/worcester/cs/kwurst/blog-rubrics/-/blob/master/ProfDevBlogEntrySpec.md) for further details.

Week (tag) | Start Date (at 00:01) | End Date (Due before 23:59)
--- | --- | ---
`Week-1` | 15 January 2024 | 21 January 2024
`Week-2` | 22 January 2024 | 28 January 2024
`Week-3` | 29 January 2024 | 4 February 2024
`Week-4` | 5 February 2024 | 11 February 2024
`Week-5` | 12 February 2024 | 18 February 2024
`Week-6` | 19 February 2024 | 25 February 2024
`Week-7` | 26 February 2024 | 3 March 2024
`Week-8` | 4 March 2024 | 10 March 2024
`Week-9` | 11 March 2024 | 17 March 2024
`Week-10` | 18 March 2024 | 24 March 2024
`Week-11` | 25 March 2024 | 31 March 2024
`Week-12` | 1 April 2024 | 7 April 2024
`Week-13` | 8 April 2024 | 14 April 2024
`Week-14` | 15 April 2024 | 21 April 2024
`Week-15` | 22 April 2024 | 28 April 2024
`Week-16` | 29 April 2024 | 5 May 2024
`Week-17` | 6 May 2024 | 12 May 2024
`Week-18` | 12 May 2024 | 20 May 2024

&copy; 2024 Karl R. Wurst, <kwurst@worcester.edu>

<img src="http://mirrors.creativecommons.org/presskit/buttons/88x31/png/by-sa.png" width=100px/>This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. To view a copy of this license, visit [http://creativecommons.org/licenses/by-sa/4.0/]() or send a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.
