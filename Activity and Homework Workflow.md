# Activity and Homework Workflow

## Workflow for In-Class Activities

### Fork the Activity to Your Team's Space

1. Go to the GitLab project for the activity. It will be in (https://gitlab.com/worcester/cs/cs-443-01-02-spring-2025/in-class-activities).
2. Click the `Fork` button in the upper right corner
3. Under `Project URL` > `Select a namespace` choose your your team’s subgroup (it will look like `worcester/cs/cs-443-01-02-spring-2025/section-XX/team-Y` - replacing XX with your section number and Y with your team number.)
4. Do not change anything else.
5. Click the `Fork Project` button.

**Only one member of your team needs to do this. You are creating one fork that all members of your team have access to.**

### Clone Your Team's Fork to Your Own Computer and Open in VSCode

 1. Make sure you are in your team’s fork. Check the “breadcrumbs” at the top of the project page on GitLab.
 2. From the blue  `Code v`  button, choose `Visual Studio Code (HTTPS)`.

     If your browser asks, allow it to open the link in Visual Studio Code.
 3. Choose the folder on your computer where you want the project saved. (You may want to create a folder that you use for all projects for this class to make it easier to find again later.)
 4. Enter your GitLab username.
 5. Enter your GitLab password.
 
     If VScode asks, let it know that you trust the authors of the files.

### Reopen the Project in a Container

This provides you with a consistent development environment will all the languages and tools that you need, regardless of which operating system is installed on your computer.

1. Make sure that Docker Desktop is running on your computer.
2. Click `Reopen in Container` in the pop-up in VS Code.

    If the pop-up has closed before you are able to click on it:

     1. Click on the `Open in a Remote Window button  ><`  in the bottom left.
     2. Choose `Reopen in Container`.
3. Wait until  `>< Dev Container: Java @ desktop-linux`  appears in the bottom left. This may take a while the first time you do this on a computer where this has not been done before. It will be faster after the first time on this same computer.

## Workflow for Homework Projects

### Fork the Project to Your Personal Space

1. Go to the GitLab project for the homework. It will be in (https://gitlab.com/worcester/cs/cs-443-01-02-spring-2025/homework).
2. Click the `Fork` button in the upper right corner
3. Under `Project URL` > `Select a namespace` choose your your personal subgroup (it will look like `worcester/cs/cs-443-01-02-spring-2025/students/XXXXX` - replacing XXXXX with your WSU username.)
4. Do not change anything else.
5. Click the `Fork Project` button.

**You are forking your own copy that only you have access to, because homework is an individual activity.**

### Clone Your Personal Fork to Your Own Computer and Open in VSCode

1. Make sure you are in your personal fork. Check the “breadcrumbs” at the top of the project page on GitLab.
2. From the blue  `Code v`  button, choose `Visual Studio Code (HTTPS)`.

    If your browser asks, allow it to open the link in Visual Studio Code.

3. Choose the folder on your computer where you want the project saved. (You may want to create a folder that you use for all projects for this class to make it easier to find again later.)

4. Enter your GitLab username.
5. Enter your GitLab password.

    If VScode asks, let it know that you trust the authors of the files.

### Reopen the Project in a Container

This provides you with a consistent development environment will all the languages and tools that you need, regardless of which operating system is installed on your computer.

1. Make sure that Docker Desktop is running on your computer.
2. Click `Reopen in Container` in the pop-up in VS Code.

    If the pop-up has closed before you are able to click on it:

     1. Click on the `Open in a Remote Window button  ><`  in the bottom left.
     2. Choose `Reopen in Container`.
3. Wait until  `>< Dev Container: Java @ desktop-linux`  appears in the bottom left. This may take a while the first time you do this on a computer where this has not been done before. It will be faster after the first time on this same computer.

#### &copy; 2025 Karl R. Wurst and Eileen B. Perez

<img src="http://mirrors.creativecommons.org/presskit/buttons/88x31/png/by-sa.png" width=100px/>This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. To view a copy of this license, visit [http://creativecommons.org/licenses/by-sa/4.0/](http://creativecommons.org/licenses/by-sa/4.0/) or send a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.