# CS-443 Software Quality Assurance and Testing

## CS-443 01, 02 &mdash; Spring 2024

### Version 2024-Spring-0.13-DRAFT, Revised 9 January 2024

**This is a DRAFT version and may be updated before the first day of class.**

## Credit and Contact Hours

3 credits

Lecture: 3 hours/week

## Catalog Course Description

> *Requirements analysis and test plan design. Testing strategies and techniques. Test coverage using statistical techniques. Code reviews and inspections.*

## Instructor

Dr. Karl R. Wurst

I prefer to be addressed, or referred to, as "Professor Wurst" or "Doctor Wurst". My pronouns are "he/him/his", although I am also fine with "they/them/their".

See [http://cs.worcester.edu/kwurst/](http://cs.worcester.edu/kwurst/) for contact information and schedule.

## Meeting Times and Location

Section | Time | Location
--- | --- | ---
01 | MW 12:30-13:45 | ST 107
02 | MW 14:00-15:15 | ST 107

## It's in the Syllabus

**If you don't find the answer to your question in the syllabus, then please ask me.**

![It's in the syllabus comic](http://www.phdcomics.com/comics/archive/phd051013s.gif)
[http://www.phdcomics.com/comics.php

**If you don't find the answer to your question in the syllabus, then please ask me.**

## Textbook

<!-- markdownlint-disable MD033 -->
<table cellpadding="1" border="0">
<tbody>
<tr>
<td><img src="http://cs.worcester.edu/kwurst/index_files/61jsKqY-gQL._SL1360_.jpg" width="110" /></td>
<td>
<p><em>A Friendly Introduction to Software Testing</em>
<br>Bill Laboon
<br><a href=https://www.amazon.com/Friendly-Introduction-Software-Testing/dp/1523477377>Print on demand from Amazon</a>
<br><a href=https://github.com/laboon/ebook>Build from source on GitHub</a>
<br><a href=https://github.com/laboon/software-testing/blob/master/software-testing-laboon-ebook.pdf>Prebuilt PDF on GitHub</a>
</td>
</tr>
<tr>
<td align="center"><a title="By Michael Reschke (self-made, following OERCommons.org&#039;s example) [Public domain], via Wikimedia Commons" href="https://commons.wikimedia.org/wiki/File%3AOERlogo.svg"><img width="100" alt="OERlogo" src="https://upload.wikimedia.org/wikipedia/commons/thumb/9/90/OERlogo.svg/256px-OERlogo.svg.png"/></a>
</td>
<td>We will be using freely available learning resources for topics in this course.
</td>
</tr>
</tbody>
</table>
<!-- markdownlint-enable MD033 -->

## Required Materials

In addition to the textbook, to successfully complete this course you will need:

1. **Laptop Computer:** You will need a laptop computer that you can bring to class sessions and can use at home. **You must bring your laptop to every class session.** The brand, specs, and operating system (Windows, Mac OS X, Linux) is unimportant – we will be using a cloud development environment.
2. **Internet Access:** You will need Internet access for access to:
   1. **Blackboard** – All course announcements, grades, and links to assignments and course materials will be made available through the course site on Blackboard.
   2. **WSU Gmail** &mdash; You must check your WSU Gmail account on a regular basis.
       * All communications made directly to individual students, such as notifications of missing assignments, or comments and needed corrections on submitted assignments will be sent to your WSU Gmail as notifications from Blackboard or GitLab.
       * All Blackboard announcements to the class, such as corrections to assignments or changes in due dates, will also be sent to your WSU Gmail account as notifications.

       **Checking your WSU Gmail on a regular basis is the best way to make sure you do not miss important, time-sensitive information from me between class sessions.**
   3. **GitLab** – Most assignments (in-class and outside of class) will be posted on and submitted through GitLab.
   4. **GitPod** – We will using GitPod to provide a cloud development environment with all the tools needed for in-class and homework assignments. You will need a GitPod account, which provides 50 hours of free development time per month.
   5. **Tutorials and articles** – I will suggest, and you will research on your own, tutorials and articles for you to learn new technologies and techniques we need.

## Where Does This Course Lead?

* CS-448 – Software Development Capstone
* Your professional career

## Course Workload Expectations

***This is a three-credit course. You should expect to spend, on average, 9 hours per week on this class.***

You will spend 3 hours per week in class. In addition, you should expect to spend, on average, at least 6 hours per week during the semester outside of class. (See *Definition of the Credit Hour*)

## Definition of the Credit Hour

>The Commission has adopted the federal definition of a credit hour: an amount of work represented in intended learning outcomes and verified by evidence of student achievement that is consistent with commonly accepted practice in postsecondary education and that reasonably approximates not less than –
>
>(1) One hour of classroom or direct faculty instruction and a minimum of two hours of out of class student work each week for approximately fifteen weeks for one semester or trimester hour of credit, or ten to twelve weeks for one quarter hour of credit, or the equivalent amount of work over a different amount of time; or
>
>(2) At least an equivalent amount of work as required in paragraph (1) of this definition for other academic activities as established by the institution including laboratory work, internships, practica, studio work, and other academic work leading to the award of credit hours.
>
>In determining the amount of work associated with a credit hour, the institution may take into account a variety of delivery methods, measurements of student work, academic calendars, disciplines, and degree levels.
>
>&mdash; New England Commission of Higher Education, [Policy on Credits and Degrees](https://www.neche.org/wp-content/uploads/2021/07/Pp111-Policy-on-Credits-and-Degrees-REVISED.pdf)

## Prerequisites

This course has a prerequisite of CS-242 – Data Structures. I expect you to be able to read a problem specification, and read, trace, and understand procedural and object-oriented code. I expect that you have had experience using a unit-testing framework and developing a set of test cases for not very complicated code. I expect that you have had experience using a version control system to fork/clone, add/commit, and push/pull.

This course has a prerequisite of CS-295 – Discrete Structures II, which has a prerequisite of CS-225 – Discrete Structures I. I expect you to be comfortable with the topics of (and formal notations for) sets and their operations, functions and relations, propositional logic and logic operators, graphs (directed and undirected), and finite state machines.

I am also expecting that many of you will have taken CS-348 – Software Process Management, and have some understanding of the phases of the software lifecycle and are comfortable using a version control system and a build system, and issuing commands and navigating your computer's file system from the command line. If you have not taken CS-348, I will post tutorials that you can complete on your own to gain the minimum competency you need to use these tools in this class.

I am also expecting that many of you will have taken MA-150 – Statistics I or MA-302 – Probability and Statistics, and are able to apply basic statistics and probability. If you have not taken MA-150 or MA-302, I will post tutorials that you can complete on your own to gain the minimum competency you need to use these tools in this class.

***If you are missing any of this background, you should not take this course.***

## Course-Level Student Learning Outcomes

Upon successful completion of this course, students will be able to:

* Design and implement comprehensive test plans and analyze requirements to determine appropriate testing strategies
* Apply a wide variety of testing techniques in an effective and efficient manner
* Compute test coverage and yield according to a variety of criteria and use statistical techniques to evaluate defect density and likelihood of faults.
* Conduct reviews and effective, efficient inspections
* Assess a software process to evaluate how effective it is at promoting quality

## LASC Student Learning Outcomes

This course does not fulfill any LASC Content Area requirements, but contributes to the following Overarching Outcomes of LASC:

* Demonstrate effective oral and written communication.
* Employ quantitative and qualitative reasoning.
* Apply skills in critical thinking.
* Apply skills in information literacy.
* Understand the roles of science and technology in our modern world.
* Understand how scholars in various disciplines approach problems and construct knowledge.
* Display socially responsible behavior and act as socially responsible agents in the world.
* Make connections across courses and disciplines.

## Software Development Concentration Student Learning Outcomes

This course addresses the following outcomes of the Software Development Concentration of the Computer Science Major:

Graduates of the Software Development Concentration will be able to (in addition to the Computer Science Major Program Learning Outcomes):

* Work with stakeholders to specify, design, develop, test, modify, and document a software system. (Introduction)
* Organize, plan, follow, and improve on, appropriate software development methodologies and team processes for a software project. (Introduction)
* Evaluate, select, and use appropriate tools for source code control, build, test, deployment, and documentation management. (Introduction)
* Evaluate, select, and apply appropriate testing techniques and tools, develop test cases, and perform software reviews. (Mastery)
* Apply professional judgement, exhibit professional behavior, and keep skills up-to-date. (Mastery)

## Program-Level Student Learning Outcomes

This course addresses the following outcomes of the Computer Science Major:

Upon successful completion of the Major in Computer Science, students will be able to:

* Analyze a problem, develop/design multiple solutions and evaluate and document the solutions based on the requirements. (Mastery)
* Communicate effectively both in written and oral form. (Mastery)
* Identify professional and ethical considerations, and apply ethical reasoning to technological solutions to problems. (Mastery)
* Demonstrate an understanding of and appreciation for the importance of negotiation, effective work habits, leadership, and good communication with teammates and stakeholders. (Mastery)
* Learn new models, techniques, and technologies as they emerge and appreciate the necessity of such continuing professional development. (Mastery)

## Course Topics

* Terminology - Error, Fault, Failure, Incident, Test, Test Case
* Test Cases - Content and Structure
* Behavioral vs Structural Testing
* Specification-Based vs Code-Based Testing
* Black-Box vs. Gray-Box vs. White/Clear-Box Testing
* Levels of Testing - Unit, Integration, System
* Static vs. Dynamic Testing
* Review of Discrete Math - Set Theory, Functions, Relations, Propositional Logic, Probability Theory, Graph Theory
* Unit Testing
  * Specification-based - Boundary Value Testing, Equivalence Class Testing, Decision Table-Based Testing
  * Code-based - Path Testing, Data Flow Testing 
  * Code Coverage
  * JUnit
* Integration Testing
  * Test Doubles
* System Testing
* Object-Oriented Testing
* Test-Driven Development
* Behavior-Driven Development
* Test Case Evaluation
* Code Reviews
* Test Automation
* Pairwise and Combinatorial Testing
* Stochastic and Property-Based Testing
* Performance Testing
* Security Testing

## Instructional Methods

This class will not be a traditional “lecture” class, and will incorporate some teaching methods that may be unfamiliar to you.

### POGIL

Rather than lecturing about the course content, you and your classmates will "discover" the content for yourselves through small-group work.

The group work will be a very structured style called Process Oriented Guided Inquiry Learning (POGIL). Through investigation of models of the concepts and answering questions that guide the team toward understanding of the models, your team will learn both the content and team process skills. In your POGIL groups each group member will have a specific role to play during the activity, and roles will be rotated so that everyone will get to experience a variety of process skills.

For more information on POGIL, see [https://pogil.org/about-pogil/what-is-pogil](https://pogil.org/about-pogil/what-is-pogil).

### HFOSS Kits

Rather than learning and practicing course content and technology using small, "made-up" examples and projects, we will use existing Humanitarian Free and Open Source Software projects. This will let you practice within an authentic, real-world context.

These Kits have been created from projects that have been "frozen" at a particular point in time, to allow specific student activities and to allow you, your classmates, and the instructor to take on the roles of the developers and maintainers of the projects.

### Competency- and Specification-Based Grading

See *Grading Policies* below.

## Grading Policies

I want everyone receiving a passing grade in this course to be, at least, minimally competent in the course learning outcomes and for that to be reflected in your course grade. Traditional grading schemes do a poor job of indicating competency.

As an example, imagine a course with two major learning outcomes: X and Y. It is widely considered that a course grade of C indicates that a student is minimally competent in achieving the course outcomes. However, if the student were to receive a grade of 100 for outcome X, and a grade of 40 for outcome Y, the student would still have a 70 (C-) average for the course. Yet the student not competent in outcome Y.

Therefore the grading in this course will be handled in a different manner:

* All assignments will be graded on a ***Meets Specification***/***Does Not Yet Meet Specification*** basis, based on whether the student work meets the instructor-supplied specification. 
* A minimum collection of assignments, indicating competency in the course learning outcomes, must be completed in a ***Meets Specification*** manner to earn a passing course grade (D).
* Higher passing grades (A, B, C) can be earned by completing more assignments and/or assignments that show higher-level thinking and learning skills.

### Assignment Grading

* All assignments in this course will be graded exclusively on a ***Meets Specification*** / ***Does Not Yet Meet Specification*** basis.
* **For each assignment, you will be given a detailed specification explaining what is required for the work to be marked *Meets Specification*.**
* Failing to meet ***any part*** of the specification will result in the work being marked **Does Not Yet Meet Specification**.
* There will be no partial credit given. 
* If you are unclear on what the specification requires, it is your responsibility to ask me for clarification.
* It will be possible to revise and resubmit a limited number of assignments with **Does Not Yet Meet Specification** grades (see *Revision and Resubmission of Work* below).

### Course Grade Determination

Your grade for the course will be determined by which assignments and/or how many assignments you complete in a *Meets Specification* manner.

#### Base Grade

<!-- markdownlint-disable MD033 -->
Assignment | Earn Base Grade<br>A | Earn Base Grade<br>B | Earn Base Grade<br>C | Earn Base Grade<br>D 
--- | :-: | :-: | :-: | :-:
Class Attendance and Participation<br>&nbsp;(out of 28 classes) | 25 | 23 | 21 | 19
Assignments<br>&nbsp;(out of 6 assignments) |
&nbsp;&nbsp;&mdash; Base Assignment | 6 | 6 | 6 | 5
&nbsp;&nbsp;&mdash; Intermediate "Add-On" |  3 | 2 | |
&nbsp;&nbsp;&mdash; Advanced "Add-On" | 2 | 1 |  |
Self-Directed Professional Development Blog Entries<br>&nbsp;(out of 16 weeks) | 8 | 6 | 4 | 2
Exam Grade Average (3 exams) | > 50% | > 50% | > 50% | &le; 50%
<!-- markdownlint-enable MD033 -->

* **Failing to meet the all the requirements for a particular letter grade will result in not earning that grade.** For example, even if you complete all other requirements for a B grade, but fail to write 6 "Self-Directed Reading Blog Entries" that *Meet Specification*, you will earn a C grade.
* **Failing to meet the all the requirements for earning a D grade will result in a failing grade for the course.**

#### Plus or Minus Grade Modifiers

* You will have a ***minus*** modifier applied to your base grade if the average of your exam grades is 65% or lower.
* You will have a ***plus*** modifier applied to your base grade if the average of your exam grades is 85% or higher.

*Notes:*

* WSU has no A+ grade.
* I reserve the right to revise *downward* the required number of assignments needed for each base grade due to changes in number of assignments assigned or unexpected difficulties with assignments.

## Class Attendance and Participation

Because a significant portion of your learning will take place during the in-class activities and as part of your team, class attendance and participation are extremely important.

For your attendance and participation to be considered to *Meet Specification*, you must:

* Arrive on time and stay for the entire class session.
* Participate fully in the in-class activity:
  * Fulfill all responsibilities of your team role.
  * Contribute to the team's learning and answers to questions.
  * Work as part of the team on the activity (not on your own.)
  * Work on the in-class activity (not some other work.)

## Assignments

The assignments will give you a chance to apply the material to different or larger tasks. The assignments will vary in what you will be asked to do - programming projects, written assignments, analysis, etc.

### Base Assignment

Every assignment will have a *base assignment* portion that must be completed for the assignment to be considered to *Meet Specification*. This will generally involve developing tests, determining suitable test inputs, and/or determining suitable test paths.

* Anyone working to earn a grade of C or higher must submit work that *Meets Specification* for **all** Base Assignments.
* Anyone working to earn a grade of D or higher must submit work that *Meets Specification* for **all but one** of the Base Assignments.

*A more complete specification for a "Meets Specification" (passing) Base Assignment will be given during with each assignment.*

### Intermediate "Add-On"

Most assignments will also have an *Intermediate Add-On* portion can be completed for anyone working for a course grade of B or higher. This will involve more detailed analysis of the developed tests, test inputs, and/or test paths.

* Differing numbers of Intermediate "Add-Ons" are required for different passing grades of B or higher. See the table under *Course Grade Determination*.

*A more complete specification for a "Meets Specification" (passing) Intermediate "Add-On" will be given during with each assignment.*

### Advanced "Add-On"

Most assignments will also have an *Advanced Add-On* portion can be completed for anyone working for a course grade of B or higher. This will involve even more detailed analysis of the developed tests, test inputs, and/or test paths.

* Differing numbers of Advanced "Add-Ons" are required for different passing grades of B or higher. See the table under *Course Grade Determination*.

*A more complete specification for a "Meets Specification" (passing) Advanced "Add-On" will be given during with each assignment.*

## Reading Assignments

In addition, there will be regular, assigned reading assignments. You are expected to complete these reading assignments by their assigned due dates, but you will not be checked on this. Completing these reading assignments will be needed to successfully complete the assignments and exam questions.

## Exams

We will have three exams. The exams will be completed outside of class time.

The first two exams will be posted on a Thursday, and will be due on the following Monday by 23:59. You may complete the exam in any 2-hour period you wish during that time.

* Exam 1 is tentatively scheduled to be posted on Thursday, 22 February 2024, and due before 23:59 on Monday, 26 February 2024.
* Exam 2 is tentatively scheduled to be posted on Thursday, 28 March 2024 and due before 23:59 on Monday, 1 April 2024.
* Exam 3 will be posted on Friday, 9 May 2024 and will be due on by 23:59 on the last day of the Final Exam period: Friday, 17 May 2024. *If you wish to take the exam during the scheduled final exam time for ANY of the sections, I will be available at that time to answer questions.*

## Self-Directed Professional Development Blog

Two of the CS Program-Level Student Learning Outcomes that this course addresses are:

> * Learn new models, techniques, and technologies as they emerge and appreciate the necessity of such continuing professional development. (Mastery)
> * Communicate effectively both in written and oral form. (Mastery)

You will be required to read outside blogs, articles, and/or books; listen to podcasts, etc. on your own and keep a blog about those items that you found useful/interesting. Your blog must be publicly accessible[^1], and will be aggregated on the [CS@Worcester Blog](http://cs.worcester.edu/blog/).

* Differing numbers of blog entries are required for different passing grades. See the table under "Course Grade Determination".
* I will only accept one blog entry per week. **You cannot wait until the end of the semester and then turn in all of your blog entries.** 

[^1]: If there is a reason why your name cannot be publicly associated with this course, you may blog under a pseudonym. You must see me to discuss the details, but your blog must still be publicly accessible and aggregated, and you must inform me of your pseudonym.

## Deliverables

All work will be submitted electronically through a variety of tools. The due date and time will be given on the assignment. The submission date and time will be determined by the submission timestamp of the tool used.

**Please do not submit assignments to me via email.** It is difficult for me to keep track of them and I often fail to remember that they are in my mailbox when it comes time to grade the assignment.

## Late Submissions

Unless otherwise specified, work is due by 9 AM ET on the day it is due.

If submitting work late, the following policy applies:

* Less than 72 hours late - No penalty.
* More than 72 hours late - Not accepted.

Quizzes and exams must be completed on time, otherwise they receive a 0.

## Revision and Resubmission of Work

### For Assignments

If you receive a ***Does Not Yet Meet Specification*** on any portion of an Assignment (Base, Intermediate Add-On, or Advanced Add-On) you may revise and resubmit the assignment ***one time only*** without the use of a token.

* **You must resubmit your corrected assignment within 7 calendar days from the date when your original assignment grade was posted.**
* You must have submitted the original assignment on time, (or within the 72 hour grace period.)
* You may ask me for clarification of the assignment, or of the comments I made on your submission.
* You may ask me to look at your revised solution to see if it addresses my comments.
* If you address all the comments in an acceptable fashion, your grade will be converted to ***Meets Specification***.
* **You must let me know by email when you resubmit the assignment, so that I know to regrade it.**

### For Self-Directed Professional Development Blog Entries

You may revise and resubmit the ***first*** blog entry on which you receive a ***Does Not Yet Meet Specification*** without the use of a token.

* **You must resubmit your corrected blog entry within 7 calendar days from the date when your original blog entry grade was posted.**
* You must have submitted the original blog entry on time, (or within the 72 hour grace period.)
* You may ask me for clarification of the comments I made on your blog entry.
* You may ask me to look at your revised blog entry to see if it addresses my comments.
* If you address all the comments in an acceptable fashion, your grade will be converted to ***Meets Specification***.
* **You must let me know by email when you have posted the revised blog entry (**with the URL to the revised blog entry**), so that I know to regrade it.

## Tokens

Each student will be able to earn up to 5 tokens over the course of the semester. These tokens will be earned by completing simple set-up and housekeeping tasks for the course.

Each token can be used to:

* replace a single missed class session (up to a maximum of 2 missed class sessions)
* turn in a second blog entry in an a single week
* revise and resubmit an assignment a second time. Any work to be revised and resubmitted must have been submitted by the original due date.
* Each unused token remaining at the end of the semester can be used to increase the exam average by 2 percentage points.

### Token Accounting

* Unused tokens will be kept track of in the Blackboard *My Grades* area.
* Tokens will not be automatically applied. You must explicitly tell me **by email** when you want to use a token, and for which assignment.

## Getting Help

If you are struggling with the material or a project please see me as soon as possible. Often a few minutes of individual attention is all that is needed to get you back on track.

By all means, try to work out the material on your own, but ask for help when you cannot do that in a reasonable amount of time. The longer you wait to ask for help, the harder it will be to catch up.

**Asking for help or coming to see me during office hours is not bothering or annoying me. I am here to help you understand the material and be successful in the course.**

## Contacting Me

You may contact me by email (<a href="mailto:Karl.Wurst@worcester.edu">Karl.Wurst@worcester.edu</a>), telephone (+1-508-929-8728), or see me in my office. My office hours are listed on the schedule on my web page (<a href="http://cs.worcester.edu/kwurst/" target="_blank">http://cs.worcester.edu/kwurst/</a>) or you may make an appointment for a mutually convenient time.

**If you email me, please include “[CS-443]” in the subject line, so that my email program can correctly file your email and ensure that your message does not get buried in my general mailbox.**

**If you email me from an account other than your Worcester State email, please be sure that your name appears somewhere in the email, so that I know who I am communicating with.** 

My goal is to get back to you within 24 hours of your email or phone call (with the exception of weekends and holidays), although you will likely hear from me much sooner. If you have not heard from me after 24 hours, please remind me.

![Cartoon with bad examples of how to send email to your instructor](http://www.phdcomics.com/comics/archive/phd042215s.gif)
[http://www.phdcomics.com/comics.php?f=1795](http://www.phdcomics.com/comics.php?f=1795)

## Code of Conduct/Classroom Civility

All students are expected to adhere to the policies as outlined in the University's Student Code of Conduct.

## Student Responsibilities 

* Contribute to a class atmosphere conducive to learning for everyone by asking/answering questions, participating in class discussions. Don’t just lurk!
* When working with a partner, participate actively. Don't let your partner do all the work - you won't learn anything that way.
* Seek help when necessary
* Start assignments as soon as they are posted.  Do not wait until the due date to seek help/to do the assignments
* Make use of the student support services (see below)
* Expect to spend at least 9 hours of work per week on classwork.
* Each student is responsible for the contents of the readings, handouts, and homework assignments.

## Names and Pronouns

If you prefer to be addressed, or referred to, by a different name than appears on your student record and/or specify your pronouns, please let me know.

If you wish this information to appear on class lists for all of your professors, you may wish to complete the [Student Chosen Name, Gender Identity, and Pronoun Usage Request Form](https://www.google.com/url?client=internal-element-cse&cx=009351802468954661311:z3yt6xnre90&q=https://www.worcester.edu/WorkArea/DownloadAsset.aspx%3Fid%3D13439&sa=U&ved=2ahUKEwjNlsyIqpLyAhXOMVkFHX29DCoQFjAAegQIBxAB&usg=AOvVaw1TtxrjeStu5KS6pRyuxh8m) and return it to the Registrar's Office.

## Additional Institutional Information

Academic Affairs maintains documents with additional information about policies and services available to students. You may access them [here](https://docs.google.com/document/d/16VoylWKdchABFTAUC5SwpxRZpFXGfiWyshgPk2_NPqg/edit?usp=sharing).

## Academic Conduct

Each student is responsible for the contents of the readings, discussions, class materials, textbook and handouts. All work must be done independently unless assigned as a group project. You may discuss assignments and materials with other students, but you should never share answers or files. **Everything that you turn in must be your own original work, unless specified otherwise in the assignment.**

Students may help each other understand the programming language and the development environment but students may not discuss actual solutions, design or implementation, to their programming assignments before they are submitted or share code or help each other debug their programming assignments. The assignments are the primary means used to teach the techniques and principles of computer programming; only by completing the programs individually will students receive the full benefit of the assignments. If you are looking at each other’s code before you submit your own, you are in violation of this policy.

Students may not use solutions to assignments from any textbooks other than the text assigned for the course, or from any person other than the instructor, or from any Internet site, or from any other source not specifically allowed by the instructor. If a student copies code from an unauthorized source and submits it as a solution to an assignment, the student will receive a 0 for that assignment.

**Any inappropriate sharing of work or use of another's work without attribution will result in a grade of zero on that assignment for all parties involved. If you do so a second time, you will receive an “E” for the course.**

Academic integrity is an essential component of a Worcester State education. Education is both the acquisition of knowledge and the development of skills that lead to further intellectual development. Faculty are expected to follow strict principles of intellectual honesty in their own scholarship; students are held to the same standard. Only by doing their own work can students gain the knowledge, skills, confidence and self-worth that come from earned success; only by learning how to gather information, to integrate it and to communicate it effectively, to identify an idea and follow it to its logical conclusion can they develop the habits of mind characteristic of educated citizens. Taking shortcuts to higher or easier grades results in a Worcester State experience that is intellectually bankrupt.

Academic integrity is important to the integrity of the Worcester State community as a whole. If Worcester State awards degrees to students who have not truly earned them, a reputation for dishonesty and incompetence will follow all of our graduates. Violators cheat their classmates out of deserved rewards and recognition. Academic dishonesty debases the institution and demeans the degree from that institution.  

It is in the interest of students, faculty, and administrators to recognize the importance of academic integrity and to ensure that academic standards at Worcester State remain strong. Only by maintaining high standards of academic honesty can we protect the value of the educational process and the credibility of the institution and its graduates in the larger community.

**You should familiarize yourself with Worcester State College’s Academic Honesty policy. The policy outlines what constitutes academic dishonesty, what sanctions may be imposed and the procedure for appealing a decision. The complete Academic Honesty Policy appears at: [http://www.worcester.edu/Academic-Policies/](http://www.worcester.edu/Academic-Policies/)**

**If you have a serious problem that prevents you from finishing an assignment on time, contact me and we'll come up with a solution.**

## Student Work Retention Policy

It is my policy to securely dispose of student work one calendar year after grades have been submitted for a course.

## Schedule

**This schedule is subject to change.**

Date | Topic | Reading
--- | --- | ---
Monday, 15 January | **Martin Luther King Day &mdash; No Class**
Wednesday, 17 January | Team Roles and Grading Schemes, Syllabus
Monday, 22 January | Testing : Purpose and Terminology | Chapters 1, 2, 3
Wednesday, 24 January | Unit Testing: Test Cases, How to Test: JUnit | Chapter 13
Monday, 29 January | Unit Testing: How to Test: JUnit, Breaking Software | Chapter 7
Wednesday, 31 January | Unit Testing: How to Test: Automation | Chapter 12
Monday, 5 February | Unit Testing: What to Test: Boundary Value Testing (Functions and Relations) | Chapter 4
Wednesday, 7 February | Unit Testing: What to Test: Equivalence Class Testing (Set Theory) | Chapter 4
Monday, 12 February | Unit Testing: What to Test: Decision Tables (Logic)
Wednesday, 14 February | Unit Testing: What to Test: Program Graphs, DD Path Testing (Graphs)
Monday, 19 February | **President's Day &mdash; No Class**
Wednesday, 21 February | Unit Testing: What to Test: Data Flow, Define/Use, Slices, OO Testing - What are Units?, MM-Path, (Graphs)
Monday, 26 February | Advanced Unit Testing: Test Doubles | Chapter 14
Wednesday, 28 February | Advanced Unit Testing: Test Doubles, Code Coverage | Chapter 14
Monday, 4 March | Integration Testing, System Testing
Wednesday, 6 March | Requirements | Chapter 5
Monday, 11 March | Test Plans | Chapter 6, 8
Wednesday, 13 March | Defects | Chapter 9
Monday, 18 March | **Spring Break &mdash; No Class**
Wednesday, 20 March | **Spring Break &mdash; No Class**
Monday, 25 March | Smoke and Acceptance Testing, Exploratory Testing | Chapter 10, 11
Wednesday, 27 March | Static Analysis
Monday, 1 April | Development: Test-Driven Development | Chapter 15
Wednesday, 3 April | Development: Test-Driven Development | Chapter 15
Monday, 8 April | Development: Writing Testable Code | Chapter 16
Wednesday, 10 April | Development: Behavior-Driven Development
Monday, 15 April | **Patriot's Day &mdash; No Class**
Wednesday, 17 April | Pairwise, Combinatorial, Mutation, Fuzzing, Stochastic and Property-Based Testing | Chapter 17, 18
Monday, 22 April | Performance Testing | Chapter 19
Wednesday, 24 April | Security Testing | Chapter 20
Monday, 29 April | Software Technical Review
Wednesday, 1 May | Interacting with Stakeholders | Chapter 21
Monday, 6 May | Slack - because things take longer than planned

Section | Scheduled Final Exam Time
:-: | :-:
**01**<br>M/W 12:30-13:45 | **Monday, 13 May 2024<br>12:30-15:30**
**02**<br>M/W 14:00-15:15 | **Wednesday, 15  May 2024<br>12:30-15:30**

<!-- markdownlint-enable MD033 -->

&copy; 2024 Karl R. Wurst, <kwurst@worcester.edu>

<!-- markdownlint-disable MD033 -->
<img src="http://mirrors.creativecommons.org/presskit/buttons/88x31/png/by-sa.png" width=100px/>This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. To view a copy of this license, visit [http://creativecommons.org/licenses/by-sa/4.0/](http://creativecommons.org/licenses/by-sa/4.0/) or send a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.
<!-- markdownlint-enable MD033 -->